# Database RSUD. Dr. Saiful Anwar
It is a simple database created with PostgreSQL RDBMS. This database was created to fulfill the task of the Binar Academy course. The theme of this database is hospital database.


# ERD
**The entity sets.**
- Hospital
- Doctor
- Patient
- Medical Record

**The attributes of the entities.**
- Hospital: Hospital's ID, name, address, doctor ID, and patient ID.
- Doctor: Doctor's ID, name, specialist, address, and phone.
- Patient: Patient's ID, name, age, address, phone, and doctor's ID.
- Medical Record: Record's ID, result of check, date of check, doctors's ID and patient's ID.

**The relationship of the entities.**
- A hospital has multiple patients, thus it is a one-to-many relationship.
- A hospital has many doctors, thus it is a one-to-many relationship.
- A doctor is associated with many patients and patient is is associated with many doctors, it is a many-to-many relationship.
- A single patient has multiple medical records and a doctor provides result of check data in multiple medical records, thus it is a one-to-many relationship.



# Lesson learned
SQL language



-- DDL

-- membuat database RSUD. Dr. Saiful Anwar
create database dbsaiful;


-- membuat tabel rumah sakit
create table hospitals (
  id bigserial primary key,
  name varchar(255) not null,
  address text not null,
  phone varchar(255) not null,
  doctor_id int not null,
  patient_id int not null
);

-- membuat tabel dokter
create table doctors (
  id bigserial primary key,
  name varchar(255) not null,
  specialist varchar(255) not null,
  address text not null,
  phone varchar(255) not null
);

-- membuat tabel pasien
create table patients (
  id bigserial primary key,
  name varchar(255) not null,
  age int not null,
  address text not null,
  phone varchar(255) not null,
  doctor_id int not null
);

-- membuat tabel rekam medis
create table medical_records (
  id bigserial primary key,
  check_result text not null,
  check_date date not null,
  doctor_id int not null,
  patient_id int not null
);



-- DML (CRUD)

-- create data rumah sakit
insert into hospitals (
  id,
  name,
  address,
  phone,
  doctor_id,
  patient_id
) values
(1, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 3, 3),
(2, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 5, 7),
(3, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 1, 1),
(4, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 4, 5),
(5, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 2, 2),
(6, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 3, 8),
(7, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 1, 4),
(8, 'RSUD. Dr. Saiful Anwar', 'Klojen', '(0341) 362101', 2, 6);

-- read data rumah sakit
-- meread/memilih seluruh data rumah sakit
select * from hospitals;

-- meread/memilih seluruh data rumah sakit yang doctor_id = 4
select * from hospitals
where doctor_id = 4;

-- update data rumah sakit
-- update data rumah sakit dokter_id = 1 yang id = 6
update hospitals
set
  doctor_id = 1
where id = 6;

-- delete data rumah sakit
-- delete data rumah sakit yang patient_id = 5
delete from hospitals
where patient_id = 5;


-- create data dokter
insert into doctors (
  id,
  name,
  specialist,
  address,
  phone
) values
(1, 'dr. M. Fauzi', 'Umum', 'Klojen', '(0341) 7761312'), 
(2, 'dr. Arif Yahya', 'Dalam', 'Klojen', '(0341) 7089147'), 
(3, 'dr. Farida Rusnianah', 'Ortopedi', 'Kedungkandang', '(0341) 7042856'), 
(4, 'dr. Catur Rustiani', 'Umum', 'Kedungkandang', '08123355094'), 
(5, 'dr. Ilyusi', 'Bedah', 'Kedungkandang', '081334441999');

-- read data dokter
-- meread/memilih seluruh data dokter
select * from doctors;

-- meread/memilih seluruh data dokter yang address = Klojen
select * from doctors
where specialist = 'Umum';

-- update data dokter
-- update data dokter Address = Klojen yang namanya = dr. Ilyusi
update doctors
set
  address = 'Klojen'
where name = 'dr. Ilyusi';

-- delete dat dokter yang id = 1
delete from doctors
where id = 1;


-- create data pasien
insert into patients (
  id,
  name,
  age,
  address,
  phone,
  doctor_id
) values
(1, 'Bunga', 22, 'Klojen', '(0341) 7761234', 1), 
(2, 'Fatih', 10, 'Klojen', '(0341) 7084321', 2), 
(3, 'Dea', 40, 'Kedungkandang', '(0341) 7040987', 3), 
(4, 'Ahmad', 65, 'Sukun', '08178905094', 1), 
(5, 'Agus', 18, 'Kedungkandang', '084567441999', 4),
(6, 'Citra', 50, 'Klojen', '087654441999', 2),
(7, 'Leo', 27, 'Kedungkandang', '08133423459', 5),
(8, 'Sam', 34, 'Sukun', '081543241999', 3);

-- read data pasien
-- meread/memilih seluruh data pasien
select * from patients;

-- meread/memilih seluruh data pasien yang age = 10
select * from patients
where age = 10;

-- update data pasien
-- update data pasien dokter_id = 3 yang namanya = Ahmad
update patients
set
  doctor_id = 3
where name = 'Ahmad';

-- delete data pasien yang phone = 087654441999
delete from patients
where phone = '087654441999';


-- create data rekam medis
insert into medical_records (
  id,
  check_result,
  check_date,
  doctor_id,
  patient_id
) values
(1, 'Demam', '22-8-2022', 1, 1), 
(2, 'TBC', '31-8-2022', 2, 2), 
(3, 'Patah Tulang', '1-9-2022', 3, 3), 
(4, 'Tifus', '1-9-2022', 1, 4), 
(5, 'DBD', '2-9-2022', 4, 5),
(6, 'Diabetes', '3-9-2022', 2, 6),
(7, 'Kanker Usus', '3-9-2022', 5, 7),
(8, 'Terapi jalan', '4-9-2022', 3, 8),
(9, 'Tifus', '10-9-2022', 1, 1),
(10, 'Operasi Ginjal', '10-9-2022', 5, 7);

-- read data rekam medis
-- meread/memilih seluruh data rekam medis
select * from medical_records;

-- meread/memilih seluruh data rekam medis yang check_date = 3-9-2022
select * from medical_records
where check_date = '3-9-2022';

-- update data rekam medis
-- update data rekam medis hasil periksa = Kanker Usus yang id = 10
update medical_records
set
  check_result = 'Operasi Kanker Usus'
where id = 10;

-- delete dat rekam medis yang id = 10
delete from medical_records
where id = 10;
